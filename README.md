# @krlwlfrt/otpc

[![pipeline status](https://img.shields.io/gitlab/pipeline/krlwlfrt/otpc.svg?style=flat-square)](https://gitlab.com/krlwlfrt/otpc/commits/master)
[![coverage](https://img.shields.io/gitlab/coverage/krlwlfrt/otpc/master?style=flat-square)](https://gitlab.com/krlwlfrt/otpc/-/pipelines)
[![npm](https://img.shields.io/npm/v/@krlwlfrt/otpc.svg?style=flat-square)](https://npmjs.com/package/@krlwlfrt/otpc)
[![license)](https://img.shields.io/npm/l/@krlwlfrt/otpc.svg?style=flat-square)](https://opensource.org/licenses/GPL-3.0)
[![documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=flat-square)](https://krlwlfrt.gitlab.io/otpc)

## Project structure

This script assumes a project structure that looks something like this:

* `./lib` for built files
* `./src` for source files
  * `./src/cli.ts` as CLI
* `./test` for test files
* Uses Typedoc for documentation
* Uses ESLint for linting
* Uses Conventional changelog with Angular style
* Uses Mocha as test framework with `@testdeck/mocha`
  * Uses NYC for test coverage

Furthermore it assumes, that the most current versions of dependencies are used.

## Documentation

See [online documentation](https://krlwlfrt.gitlab.io/otpc) for detailed API description.
