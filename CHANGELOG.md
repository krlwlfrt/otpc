# [2.0.0](https://gitlab.com/krlwlfrt/otpc/compare/v1.0.0...v2.0.0) (2024-11-09)



# [1.0.0](https://gitlab.com/krlwlfrt/otpc/compare/v0.5.1...v1.0.0) (2023-02-28)



## [0.5.1](https://gitlab.com/krlwlfrt/otpc/compare/v0.5.0...v0.5.1) (2022-09-07)



# [0.5.0](https://gitlab.com/krlwlfrt/otpc/compare/v0.4.0...v0.5.0) (2022-09-07)



# [0.4.0](https://gitlab.com/krlwlfrt/otpc/compare/v0.3.0...v0.4.0) (2021-04-14)


### Bug Fixes

* add typedoc to dev dependencies ([ecadafc](https://gitlab.com/krlwlfrt/otpc/commit/ecadafc1d5cb293a2fd32995a87e9c9d5419bfde))


### Features

* add warnings for bugs and repository entries ([2b2a57e](https://gitlab.com/krlwlfrt/otpc/commit/2b2a57e31bda028681d404f76106ec8d4dca88aa))



# [0.3.0](https://gitlab.com/krlwlfrt/otpc/compare/v0.2.0...v0.3.0) (2021-03-12)


### Features

* make usage of explicit any an error ([a84f4f2](https://gitlab.com/krlwlfrt/otpc/commit/a84f4f2a161727e913e4977f534963cf13fc791f))



# [0.2.0](https://gitlab.com/krlwlfrt/otpc/compare/v0.1.3...v0.2.0) (2021-03-10)


### Bug Fixes

* correctly handle results of confirms ([a6970af](https://gitlab.com/krlwlfrt/otpc/commit/a6970af3c96518c0c83ea6e70a33475a33ef85e3))
* initialize configs in case they are empty ([2e3cde9](https://gitlab.com/krlwlfrt/otpc/commit/2e3cde90c8d12f61fb0aafe78438daf9d00a2f1d))


### Features

* add rule to ensure correct spacing around curly brackets ([facfc64](https://gitlab.com/krlwlfrt/otpc/commit/facfc64d5da2100a1b011e577e93bb769188433e))



## [0.1.3](https://gitlab.com/krlwlfrt/otpc/compare/v0.1.2...v0.1.3) (2021-03-05)


### Bug Fixes

* show missing dev dependencies last ([8a2e437](https://gitlab.com/krlwlfrt/otpc/commit/8a2e437119f325837d50c77e9aa28da8ce0f3630))
* show non-primitive properties inline ([5ca6528](https://gitlab.com/krlwlfrt/otpc/commit/5ca65289be94efa46c33488bcb39514d6f57aa2c))



## [0.1.2](https://gitlab.com/krlwlfrt/otpc/compare/v0.1.1...v0.1.2) (2021-03-05)


### Bug Fixes

* compare entries in object by stringifying them ([f6d518a](https://gitlab.com/krlwlfrt/otpc/commit/f6d518a6c57bc5ada2bae89fdb55cfa503848753))



## [0.1.1](https://gitlab.com/krlwlfrt/otpc/compare/v0.1.0...v0.1.1) (2021-03-05)


### Bug Fixes

* adjust NYC configuration ([e8c484c](https://gitlab.com/krlwlfrt/otpc/commit/e8c484c3631c88b849e44285a11cb3520252f3f6))



# [0.1.0](https://gitlab.com/krlwlfrt/otpc/compare/v0.0.2...v0.1.0) (2021-03-05)


### Bug Fixes

* rename dot files because they are excluded by npm ([65bf2b2](https://gitlab.com/krlwlfrt/otpc/commit/65bf2b28a4df1dfa213bcef70cc18d23ddfbef51))


### Features

* add adjustment for .eslintrc ([ca56eb7](https://gitlab.com/krlwlfrt/otpc/commit/ca56eb7bda8559d1b9c12170e517b950577f16f6))



## [0.0.2](https://gitlab.com/krlwlfrt/otpc/compare/v0.0.1...v0.0.2) (2021-03-05)


### Bug Fixes

* make sure to ship config/ignore files ([5fc841e](https://gitlab.com/krlwlfrt/otpc/commit/5fc841e0cc0d9f66c5c2deef7df0781aae59df4a))



## [0.0.1](https://gitlab.com/krlwlfrt/otpc/compare/4c4e1d373eb96c41c938a3af9f410abad8ef8423...v0.0.1) (2021-03-05)


### Bug Fixes

* add missing dependency ([029d5ca](https://gitlab.com/krlwlfrt/otpc/commit/029d5ca41ca9f6251e8f0275cb091daebb4a95cf))
* reset version ([d3f12f4](https://gitlab.com/krlwlfrt/otpc/commit/d3f12f4a55adff74ca8aa53ae3b24462946c5a4f))


### Features

* add initial implementation ([4c4e1d3](https://gitlab.com/krlwlfrt/otpc/commit/4c4e1d373eb96c41c938a3af9f410abad8ef8423))



