import {PackageJson, TsConfigJson} from 'type-fest';

export const defaultPackageJson: PackageJson = {
  license: 'GPL-3.0-only',
  main: 'lib/index.js',
  nyc: {
    all: true,
    branches: 95,
    'check-coverage': true,
    exclude: ['src/cli.ts'],
    extension: ['.ts'],
    functions: 95,
    include: ['src'],
    lines: 95,
    'per-file': true,
    reporter: ['html', 'text'],
    require: ['ts-node/register'],
    statements: 95,
  },
  scripts: {
    build: 'npm run lint && npm run compile',
    clean: 'rm -rf package-lock.json node_modules',
    compile: 'rimraf lib && tsc && prepend lib/cli.js \'#!/usr/bin/env node\n\'',
    documentation: 'typedoc --out docs --readme README.md src/index.ts',
    lint: 'eslint src',
    prepublishOnly: 'npm ci && npm run build && npm test',
    push: 'git push && git push origin "v$npm_package_version"',
    test: 'nyc mocha --require ts-node/register \'test/**/*.spec.ts\'',
    version:
      'conventional-changelog -p angular -i CHANGELOG.md -s -r 0 && git add CHANGELOG.md',
  },
  types: 'lib/index.d.ts',
};

export const defaultTsconfigJson: TsConfigJson & {
  compilerOptions: {
    extendedDiagnostics: boolean;
  };
} = {
  compilerOptions: {
    allowUnreachableCode: false,
    allowUnusedLabels: false,
    alwaysStrict: true,
    diagnostics: true,
    declaration: true,
    esModuleInterop: true,
    experimentalDecorators: true,
    extendedDiagnostics: true,
    forceConsistentCasingInFileNames: true,
    incremental: true,
    inlineSourceMap: true,
    listEmittedFiles: true,
    module: 'CommonJS',
    moduleResolution: 'node',
    newLine: 'lf',
    noErrorTruncation: true,
    noFallthroughCasesInSwitch: true,
    noImplicitAny: true,
    noImplicitReturns: true,
    noImplicitThis: true,
    noUnusedLocals: true,
    noUnusedParameters: true,
    outDir: './lib/',
    pretty: true,
    removeComments: true,
    strict: true,
    strictBindCallApply: true,
    strictFunctionTypes: true,
    strictNullChecks: true,
    strictPropertyInitialization: true,
    target: 'es6',
  },
  exclude: ['./lib/', './test/'],
};

export const devDependencies = [
  '@eslint/js',
  '@types/eslint__js',
  '@testdeck/mocha',
  '@types/node',
  'conventional-changelog-cli',
  'eslint',
  'eslint-plugin-jsdoc',
  'mocha',
  'nyc',
  'prepend-file-cli',
  'rimraf',
  'ts-node',
  'typedoc',
  'typescript',
  'typescript-eslint',
];
