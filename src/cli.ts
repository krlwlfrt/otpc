import {deepStrictEqual} from 'assert';
import {Command} from 'commander';
import {existsSync, readFileSync} from 'fs';
import {readFile, writeFile} from 'fs/promises';
import inquirer from 'inquirer';
import {join, resolve} from 'path';
import {PackageJson} from 'type-fest';
import {
  defaultPackageJson,
  defaultTsconfigJson,
  devDependencies,
} from './defaults';

/**
 * Indentation for code
 */
const INDENTATION = 2;

/**
 * Initialize command
 */
const command = new Command('otpc');

/**
 * Helper function to get answer from inquirer result
 *
 * TypeScript definitions and documentation do not match implementation - therefore this helper is needed.
 * @param question Question to ask
 * @param inquire Whether or not to ask the question
 * @returns Whether or not the question was accepted
 */
async function getAnswer(question: string, inquire: boolean): Promise<boolean> {
  if (!inquire) {
    return true;
  }

  const result = await inquirer.prompt({
    name: 'confirm',
    type: 'confirm',
    message: question,
  });

  if (typeof result === 'boolean') {
    return result;
  }

  if (typeof result === 'object' && typeof result.confirm === 'boolean') {
    return result.confirm;
  }

  if (
    typeof result === 'object' &&
    typeof result.confirm === 'object' &&
    typeof result.confirm['json?'] === 'boolean'
  ) {
    return result.confirm['json?'];
  }

  throw new Error('Answer could not be determined.');
}

/**
 * Adjust an object
 * @param sourceObject Source object to use for adjustments
 * @param targetObject Target object to adjust
 * @param inquire Whether or not to inquire about making changes
 * @param objectName Name for object
 * @param propertyName Name for properties
 * @returns The adjusted object
 */
async function adjustObject(
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  sourceObject: any,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  targetObject: any,
  inquire: boolean,
  objectName = 'object',
  propertyName = 'property',
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Promise<any> {
  if (Array.isArray(sourceObject) && Array.isArray(targetObject)) {
    return adjustArray(
      sourceObject,
      targetObject,
      inquire,
      'superset',
      objectName,
    );
  }

  for (const property in sourceObject) {
    if (!Object.prototype.hasOwnProperty.call(sourceObject, property)) {
      continue;
    }

    if (
      JSON.stringify(targetObject[property]) ===
      JSON.stringify(sourceObject[property])
    ) {
      continue;
    }

    if (typeof sourceObject[property] === 'object') {
      await adjustObject(
        sourceObject[property],
        targetObject[property],
        inquire,
        objectName,
        property,
      );
      continue;
    }

    if (
      await getAnswer(
        `Adjust ${propertyName} '${property}' in ${objectName} to '${JSON.stringify(sourceObject[property])}'?`,
        inquire,
      )
    ) {
      targetObject[property] = sourceObject[property];
    }
  }
}

/**
 * Adjust an array
 * @param sourceArray Source array to use for adjustments
 * @param targetArray Target array to adjust
 * @param inquire Whether or not to inquire about making changes
 * @param mode Adjustment mode
 * @param arrayName Name for array
 */
async function adjustArray(
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  sourceArray: any[],
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  targetArray: any[],
  inquire: boolean,
  mode: 'superset',
  arrayName = 'array',
) {
  for (const entry of sourceArray) {
    if (
      mode === 'superset' &&
      targetArray.some((entryInTarget) => entryInTarget === entry)
    ) {
      continue;
    }

    if (
      await getAnswer(
        `Add entry '${JSON.stringify(entry, null, INDENTATION)}' to '${arrayName}'?`,
        inquire,
      )
    ) {
      targetArray.push(entry);
    }
  }
}

/**
 * Save a changed object
 * @param object Object to save
 * @param path Path to save object to
 * @param inquire Whether or not to inquire about making changes
 * @param objectName Name for object
 * @param originalObject Original object for comparison
 */
async function saveChangedObject(
  object: unknown,
  path: string,
  inquire: boolean,
  objectName = 'object',
  originalObject = {},
) {
  try {
    deepStrictEqual(object, originalObject);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  } catch (e: any) {
    console.log(e.message);

    if (await getAnswer(`Save adjusted ${objectName}?`, inquire)) {
      await writeFile(path, `${JSON.stringify(object, null, INDENTATION)}\n`);
    }
  }
}

/**
 * Adjust a file
 * @param sourceFile Source file to use for adjustments
 * @param targetFile Target file to adjust
 * @param inquire Whether or not to inquire about making changes
 * @param fileName Name of the file to be adjusted
 */
async function adjustFile(
  sourceFile: string,
  targetFile: string,
  inquire: boolean,
  fileName = 'file',
) {
  const sourceFileContent = (await readFile(sourceFile)).toString();
  let targetFileContent = '';
  try {
    targetFileContent = (await readFile(targetFile)).toString();
    // eslint-disable-next-line
  } catch (e: any) {
    // noop
  }

  try {
    deepStrictEqual(sourceFileContent, targetFileContent);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  } catch (e: any) {
    console.log(e.message);

    if (await getAnswer(`Save adjusted ${fileName}?`, inquire)) {
      await writeFile(targetFile, sourceFileContent);
    }
  }
}

command
  .version(
    JSON.parse(readFileSync(join(__dirname, '..', 'package.json')).toString())
      .version,
  )
  .arguments('<projectPath>')
  .description('Adjust configuration', {
    projectPath: 'Path of project to adjust config for',
  })
  .option('-y, --yes', 'Do not inquire about making changes', false)
  .action(async (relativeProjectPath) => {
    const projectPath = resolve(relativeProjectPath);

    const packageJsonContent = (
      await readFile(join(projectPath, 'package.json'))
    ).toString();
    const originalPackageJson: PackageJson = JSON.parse(packageJsonContent);
    const packageJson = JSON.parse(packageJsonContent) as PackageJson;

    let tsconfigJsonContent = '{}';
    if (existsSync(join(projectPath, 'tsconfig.json'))) {
      tsconfigJsonContent = (
        await readFile(join(projectPath, 'tsconfig.json'))
      ).toString();
    }
    const originalTsconfigJson = JSON.parse(tsconfigJsonContent);
    const tsconfigJson = JSON.parse(tsconfigJsonContent);

    if (typeof packageJson.scripts === 'undefined') {
      packageJson.scripts = {};
    }
    if (typeof packageJson.nyc === 'undefined') {
      packageJson.nyc = {};
    }
    if (typeof packageJson.devDependencies === 'undefined') {
      packageJson.devDependencies = {};
    }

    if (typeof tsconfigJson.compilerOptions === 'undefined') {
      tsconfigJson.compilerOptions = {};
    }
    if (typeof tsconfigJson.exclude === 'undefined') {
      tsconfigJson.exclude = [];
    }

    await adjustObject(
      defaultPackageJson,
      packageJson,
      !command.opts().yes,
      'package.json',
      'script',
    );

    await adjustObject(
      defaultTsconfigJson.compilerOptions,
      tsconfigJson.compilerOptions,
      !command.opts().yes,
      'tsconfig.json',
      'compiler option',
    );
    await adjustArray(
      defaultTsconfigJson.exclude as string[],
      tsconfigJson.exclude,
      !command.opts().yes,
      'superset',
      'tsconfig.json excludes',
    );

    await saveChangedObject(
      packageJson,
      join(projectPath, 'package.json'),
      !command.opts().yes,
      'package.json',
      originalPackageJson,
    );
    await saveChangedObject(
      tsconfigJson,
      join(projectPath, 'tsconfig.json'),
      !command.opts().yes,
      'tsconfig.json',
      originalTsconfigJson,
    );

    const missingDevDependencies: string[] = [];
    for (const name of devDependencies) {
      if (!(name in packageJson.devDependencies)) {
        missingDevDependencies.push(name);
      }
    }

    await adjustFile(
      join(__dirname, '..', 'resources', 'editorconfig'),
      join(projectPath, '.editorconfig'),
      !command.opts().yes,
      '.editorconfig',
    );
    await adjustFile(
      join(__dirname, '..', 'resources', 'eslint.config.mjs'),
      join(projectPath, 'eslint.config.mjs'),
      !command.opts().yes,
      'eslint.config.mjs',
    );
    await adjustFile(
      join(__dirname, '..', 'resources', 'gitignore'),
      join(projectPath, '.gitignore'),
      !command.opts().yes,
      '.gitignore',
    );
    await adjustFile(
      join(__dirname, '..', 'resources', 'npmignore'),
      join(projectPath, '.npmignore'),
      !command.opts().yes,
      '.npmignore',
    );

    if (missingDevDependencies.length > 0) {
      console.log(`Missing the following dev dependencies: ${missingDevDependencies.join(',')}
Please install using the following command:

npm install --save-dev --save-exact ${missingDevDependencies.join(' ')}`);
    }

    if (typeof packageJson.bugs === 'undefined') {
      console.log(
        'Missing an entry for bugs in the package.json. Please consider to add it.',
      );
    }

    if (typeof packageJson.repository === 'undefined') {
      console.log(
        'Missing an entry for repository in the package.json. Please consider to add it.',
      );
    }
  });

command.parse(process.argv);
